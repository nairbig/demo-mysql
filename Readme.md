# Final Project

# Kelompok 14

# Anggota Kelompok

<ul>
    <li>Ikhwan Nur Hidayat (@IkhwanNur)</li>
    <li>Teuku Gibrian Laksamana (@TLaks)</li>
    <li>Muhammad Aulia Rizki (@MuhammadAuliaRizki)</li>
</ul>

# Tema Project
<p> Perpustakaan </p>

# ERD
<img src="ERD14_Perpustakaan.png" alt="erd">

# Link Video
<a href="https://www.youtube.com/watch?v=ScpPFEg-3KM">Video Kelompok 14</a>

<a href="https://www.heroku.com">Link Deploy Kelompok 14</a>